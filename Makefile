clean:
	rm -rf public

prepare:
	rm -rf docs
	cp -R source docs	
	groovy scripts/antora.groovy 

antora:	
	antora generate local.yml	

assets:	
	cp -R static/* public/_

all: clean prepare antora assets